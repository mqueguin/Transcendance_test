import { createApp } from 'vue'
import App from './App.vue'
import * as VueRouter from 'vue-router'
import LoginComponent from './components/LoginComponent.vue'

const router = VueRouter.createRouter({
    history: VueRouter.createWebHistory(),
    routes: [
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/login',
            name: 'login',
            component: LoginComponent,
            props: true
        }
        // Add others routes here !
    ]
})

createApp(App).use(router).mount('#app')
